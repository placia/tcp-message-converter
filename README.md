### 설명
TcpMessageConverter는 전문 송수신 통신에서 쉽게 전문을 만들고 파싱하게하는 기능을 제공하는 라이브러이다.

#### 전문필드
전문에 포함되기 위해서는 전문에 사용하는 DTO 멤버변수에 @Column을 선언해야 한다.

* 만약 전문길이 필드를 자동으로 처리하려면 전문길이 필드에 @MsgLenColumn을 선언하고 computeLength 메소드를 호출해야 한다.

* 오른쪽 정렬을 적용하기 위해서는 (앞자리 패딩) @Column annotation에 rightJustify를 true로 선언한다.

* 패딩은 @Column annotation에 padding으로 패딩에 사용한 문자를 입력한다.

* 전문내 반복되는 항목이 있는 경우 반복횟수 필드에 @Column annoation의 repeat를 true로 선언하고
반복필드(리스트)의 반복횟수(size)를 값으로 입력해야 한다. (해당값의 횟수에 따라 반복전문 길이 결정되나 실제적인 전문은 리스트에 추가된 데이터만큼 전문내용을 생성한다.)

* 반복되는 항목(리스트)에 @Column을 선언할 때 한개 값 길이만큼으로 len을 선언한다.

##### 전문 생성 샘플

```java
public class MessageClass {
    @Column(len = 4, rightJustify = true, padding = "0")
    @MsgLenColumn
    private int len;
    @Column(len = 10)
    private String message;
    @Column(len = 100)
    private List<DetailMessage> detailMessageList;
}

public class DetailMessage {
    @Column(len = 100)
    private String detailMessage;
}

//값 셋팅
MessageClass messageClass = new MessageClass();

//전문생성
String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(messageClass, len -> len - 4);

```
* computeLength 메소드는 전문생성시 전문길이를 자동으로 셋팅하기 위해서 반다시 호출해야 한다.
일반적으로 전문길이는 헤더길이는 제외 또는 전문길이 필드 제외 등 규격마다 상이하다. 

* 규격에 맞추기 위해서 computeLength 메소드 호출시 function 클래스를 파라미터로 제공할 수 있다. function 클래스로
전문 규격에 맞는 길이로 조정한다.

```java
//전문길이 필드 제외(전문길이 필드 길이: 4)
String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(messageClass, len -> len - 4);

```

##### 전문 파싱 샘플

```java
String targetMsg = ""; //수신 전문 메시지

MessageClass messageClass = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .decode(targetMsg, MessageClass.class);
```

#### 전문생성, 파싱 전후 처리
public network에서 전문통신을 할 때 생성된 전문에 암복호화 처리를 할 때가 있다. 
이러한 전문생성 후처리, 전문파싱 전처리에 대한 기능을 제공하기 위해 MessageHandler 인터페이스를 제공한다.
후처리, 전처리를 위해서 MessageHandler를 상속받아 구현 후 전문생성, 파싱전에 addMessageHandler를 통해 등록한다.

기본적인 AES 암복호화를 위해 MessageAesCryptoHandler 클래스를 제공하고 있다.

* 전문생성 후처리는 MessageHandler를 상속받은 클래스에서 executeAfterEncode 메소드를 구현한다.

* 전문파싱 전처리는 MessageHandler를 상속받은 클래스에서 executeBeforeDecode 메소드를 구현한다.

##### 샘플
```java
MessageAesCryptoHandler messageHandler = new MessageAesCryptoHandler(key, iv, "UTF-8", 
    MessageAesCryptoHandler.MessageType.BASE64)

String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandler(messageHandler)
                .build()
                .encode(messageClass, len -> len - 4);

MessageClass messageClass = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandler(messageHandler)
                .build()
                .decode(targetMsg, MessageClass.class);
```
#### enum type
Enum을 사용한 전문을 생성, 파싱을 지원한다. 그러나 String enum을 사용한다면 toString() 메소드를 구현하여 string value을
리턴하여야 한다.

```java
public enum EnumType {
    VALUE_1("A");
    
    private String value;
    private EnumType(String value) {
        this.value = value;
    }
    
    public String toString() {
        return this.value;
    }
}
```

#### 참고
반복되는 필드인 경우 Object element를 가진 List 종류만 지원한다. 

만약 ```List<String>```과 같은 element가 primitive인 리스트와 ```List<Map<String, String>>``` 같은 element가 ParameterizedType은 지원하지 않는다.

그리고 필드항목 중에 Map type도 지원하지 않는다.