package com.payment.skpay;

import com.payment.skpay.annotation.Column;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.*;

public class MessageDecoderTest {
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void decode_싱글클래스_테스트() {
        String message = "00161000싱글  ";

        MessageDecoder decoder = new MessageDecoder(Charset.defaultCharset().name());
        Mock.SingleClass singleClass = decoder.decode(message, Mock.SingleClass.class);

        assertNotNull(singleClass);
        assertEquals(16, singleClass.integer_number);
        assertEquals(1000, singleClass.long_number);
        assertEquals("싱글", singleClass.str);
        assertNull(singleClass.lists);
    }

    @Test
    public void decode_싱글클래스_리스트_1_테스트() {
        String message = "00161000싱글  element ";

        MessageDecoder decoder = new MessageDecoder(Charset.defaultCharset().name());
        Mock.SingleClass singleClass = decoder.decode(message, Mock.SingleClass.class);

        assertNotNull(singleClass);
        assertEquals(16, singleClass.integer_number);
        assertEquals(1000, singleClass.long_number);
        assertEquals("싱글", singleClass.str);
        assertNotNull(singleClass.lists);
        assertEquals("element", singleClass.lists.get(0).str);
    }

    @Test
    public void decode_싱글클래스_리스트_2_테스트() {
        String message = "02element two     ";

        MessageDecoder decoder = new MessageDecoder(Charset.defaultCharset().name());
        ListTest listTest = decoder.decode(message, ListTest.class);

        assertNotNull(listTest);
        assertEquals(2, listTest.cnt);

        assertNotNull(listTest.lists);
        assertEquals("element", listTest.lists.get(0).str);
        assertEquals("two", listTest.lists.get(1).str);
    }

    @Test
    public void decode_상속클래스_테스트() {
        String message = "00161000상속  element 00162000TEST    element ";

        MessageDecoder decoder = new MessageDecoder(Charset.defaultCharset().name());

        Mock.SubClass subClass = decoder.decode(message, Mock.SubClass.class);
        assertNotNull(subClass);
        assertEquals(16, subClass.super_integer_number);
        assertEquals(1000, subClass.super_long_number);
        assertEquals("상속", subClass.super_str);

        assertNotNull(subClass.super_lists);
        assertEquals("element", subClass.super_lists.get(0).str);

        assertEquals(16, subClass.integer_number);
        assertEquals(2000, subClass.long_number);
        assertEquals("TEST", subClass.str);
        assertEquals("element", subClass.lists.get(0).str);
    }

    public static class ListTest {
        @Column(len = 2, repeat = true, rightJustify = true, padding = "0")
        public int cnt;
        @Column(len = 8)
        public List<Mock.ListElementClass> lists;
    }
}