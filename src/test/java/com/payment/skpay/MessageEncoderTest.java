package com.payment.skpay;

import com.payment.skpay.exception.ColumnEncodeException;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.Charset;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class MessageEncoderTest {

    @Before
    public void setUp() throws Exception {
        FieldCache fieldCache = FieldCache.getInstance();
        fieldCache.get(Mock.SingleClass.class);
        fieldCache.get(Mock.SubClass.class);
        fieldCache.get(Mock.InPOJOFielcClass.class);
    }

    @Test
    public void sizeOf_싱글클래스_리스트없음() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        int size = encoder.sizeOf(new Mock.SingleClass());
        assertEquals(16, size);
    }

    @Test
    public void sizeOf_싱글클래스_리스트() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SingleClass singleClass = new Mock.SingleClass();
        singleClass.lists = new ArrayList<>();

        int size = encoder.sizeOf(singleClass);
        assertEquals(16, size);

        singleClass.lists.add(new Mock.ListElementClass());

        size = encoder.sizeOf(singleClass);
        assertEquals(24, size);

        singleClass.lists.add(new Mock.ListElementClass());

        size = encoder.sizeOf(singleClass);
        assertEquals(32, size);
    }

    @Test
    public void sizeOf_상속클래스() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SubClass subClass = new Mock.SubClass();
        int size = encoder.sizeOf(subClass);

        assertEquals(32, size);
    }

    @Test
    public void sizeOf_필드내POJO() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.InPOJOFielcClass inPOJOFielcClass = new Mock.InPOJOFielcClass();
        inPOJOFielcClass.innerClass = new Mock.InnerClass();

        int size = encoder.sizeOf(inPOJOFielcClass);

        assertEquals(16, size);
    }

    @Test
    public void setLenValue_싱글클래스() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SingleClass singleClass = new Mock.SingleClass();
        int size = encoder.sizeOf(singleClass);
        encoder.setLenValue(size, singleClass);

        assertEquals(16, singleClass.integer_number);
    }

    @Test
    public void setLenValue_상속클래스() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SubClass subClass = new Mock.SubClass();
        int size = encoder.sizeOf(subClass);
        encoder.setLenValue(size, subClass);

        assertEquals(32, subClass.super_integer_number);
    }

    //메시지의 POJO는 grouping 성격의 필드이므로 POJO 필드내 MsgLenColumn은 동작하지 않는다.
    @Test(expected = ColumnEncodeException.class)
    public void setLenValue_필드내POJO() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.InPOJOFielcClass inPOJOFielcClass = new Mock.InPOJOFielcClass();
        inPOJOFielcClass.innerClass = new Mock.InnerClass();

        int size = encoder.sizeOf(inPOJOFielcClass);
        encoder.setLenValue(size, inPOJOFielcClass);
    }

    @Test
    public void encode_싱글클래스() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SingleClass singleClass = new Mock.SingleClass();
        singleClass.integer_number = 16;
        singleClass.long_number = 1000;
        singleClass.str = "싱글";

        String result = encoder.encode(singleClass);
        assertEquals("00161000싱글  ", result);
    }

    @Test
    public void encode_싱글클래스_리스트() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SingleClass singleClass = new Mock.SingleClass();
        singleClass.integer_number = 16;
        singleClass.long_number = 1000;
        singleClass.str = "싱글";
        singleClass.lists = new ArrayList<>();
        singleClass.lists.add(new Mock.ListElementClass());

        String result = encoder.encode(singleClass);
        assertEquals("00161000싱글  element ", result);
    }

    @Test
    public void encode_상속클래스() {
        MessageEncoder encoder = new MessageEncoder(Charset.defaultCharset().name());

        Mock.SubClass subClass = new Mock.SubClass();
        subClass.super_integer_number = 16;
        subClass.super_long_number = 1000;
        subClass.super_str = "상속";
        subClass.super_lists = new ArrayList<>();
        subClass.super_lists.add(new Mock.ListElementClass());
        subClass.integer_number = 16;
        subClass.long_number = 2000;
        subClass.str = "TEST";
        subClass.lists = new ArrayList<>();
        subClass.lists.add(new Mock.ListElementClass());

        String result = encoder.encode(subClass);

        assertEquals("00161000상속  element 00162000TEST    element ", result);
    }
}