package com.payment.skpay;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.annotation.MsgLenColumn;
import com.payment.skpay.handler.MessageAesCryptoHandler;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TcpMessageConverterTest {
    EncodeSingleClass encodeSingleClass;
    EncodeSingleWithObjectClass encodeSingleWithObjectClass;
    HierachySubClass hierachySubClass;
    ListInClass listInClass;

    @Before
    public void setUp() throws Exception {
        encodeSingleClass = new EncodeSingleClass();
        encodeSingleClass.integer_type = 1000;
        encodeSingleClass.long_type = 2000;
        encodeSingleClass.double_type = 3000;
        encodeSingleClass.string_type = "ENG";
        encodeSingleClass.enumType = EnumType.VALUE_1;

        encodeSingleWithObjectClass = new EncodeSingleWithObjectClass();
        encodeSingleWithObjectClass.integer_type = 1000;
        encodeSingleWithObjectClass.long_type = 2000;
        encodeSingleWithObjectClass.double_type = 3000;
        encodeSingleWithObjectClass.string_type = "ENG";
        encodeSingleWithObjectClass.withObject = new WithObject();
        encodeSingleWithObjectClass.withObject.with_object_string_type = "한글";

        hierachySubClass = new HierachySubClass();
        hierachySubClass.string_type = "ENG";
        hierachySubClass.integer_type = 1000;
        hierachySubClass.sub_string_type = "한글";

        listInClass = new ListInClass();
        listInClass.string_type = "ENG";
        listInClass.repeat = 2;
        listInClass.list = new ArrayList<>();

        WithObject list_item1 = new WithObject();
        list_item1.with_object_string_type = "한글";
        listInClass.list.add(list_item1);

        WithObject list_item2 = new WithObject();
        list_item2.with_object_string_type = "한글ENG";
        listInClass.list.add(list_item2);
    }

    @Test
    public void 오브젝트_인코딩_테스트() {
        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(encodeSingleClass, len -> len - 4);

        assertEquals("00500000001000000000200000003000.0ENG                 VALUE_1   ", result);
    }

    @Test
    public void 오브젝트필드_클래스_인코딩_테스트() {
        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(encodeSingleWithObjectClass, len -> len - 4);

        assertEquals("00600000001000000000200000003000.0ENG                 한글    ", result);
    }

    @Test
    public void 상속클래스_인코딩_테스트() {
        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(hierachySubClass, len -> len - 8);

        assertEquals("0030ENG 0000001000한글              ", result);
    }

    @Test
    public void 반복필드_클래스_인코딩_테스트() {
        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .encode(listInClass, len -> len - 4);

        assertEquals("0026ENG 02한글    한글ENG ", result);
    }

    //////////////////

    @Test
    public void 오브젝트_디코딩_테스트() {
        String targetMsg = "00500000001000000000200000003000.0ENG                 VALUE_1   ";
        EncodeSingleClass result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .decode(targetMsg, EncodeSingleClass.class);

        assertNotNull(result);
        assertEquals(1000, result.integer_type);
        assertEquals(2000, result.long_type);
        assertEquals(3000, result.double_type, 0);
        assertEquals("ENG", result.string_type);
        assertEquals(EnumType.VALUE_1, result.enumType);
    }

    @Test
    public void 오브젝트필드_클래스_디코딩_테스트() {
        String targetMsg = "00600000001000000000200000003000.0ENG                 한글    ";
        EncodeSingleWithObjectClass result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .decode(targetMsg, EncodeSingleWithObjectClass.class);

        assertNotNull(result);
        assertEquals(1000, result.integer_type);
        assertEquals(2000, result.long_type);
        assertEquals(3000, result.double_type, 0);
        assertEquals("ENG", result.string_type);

        assertNotNull(result.withObject);
        assertEquals("한글", result.withObject.with_object_string_type);
    }

    @Test
    public void 상속클래스_디코딩_테스트() {
        String targetMsg = "0030ENG 0000001000한글              ";

        HierachySubClass result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .decode(targetMsg, HierachySubClass.class);

        assertNotNull(result);
        assertEquals("ENG", result.string_type);
        assertEquals(1000, result.integer_type);
        assertEquals("한글", result.sub_string_type);
    }

    @Test
    public void 반복필드_클래스_디코딩_테스트() {
        String targetMsg = "0026ENG 02한글    한글ENG ";
        ListInClass result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .build()
                .decode(targetMsg, ListInClass.class);

        assertNotNull(result);
        assertEquals(26, result.len);
        assertEquals("ENG", result.string_type);
        assertEquals(2, result.repeat);

        assertNotNull(result.list);
        assertEquals("한글", result.list.get(0).with_object_string_type);
        assertEquals("한글ENG", result.list.get(1).with_object_string_type);
    }

    @Test
    public void 메시지핸들러_BASE64_테스트() {
        byte[] key = "1234567890123456".getBytes();
        byte[] iv = new byte[16];

        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandlers(new MessageAesCryptoHandler(key, iv, "UTF-8", MessageAesCryptoHandler.MessageType.BASE64))
                .build()
                .encode(listInClass, len -> len - 4);

        System.out.println(result);

        ListInClass listInClass = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandlers(new MessageAesCryptoHandler(key, iv, "UTF-8", MessageAesCryptoHandler.MessageType.BASE64))
                .build()
                .decode(result, ListInClass.class);

        assertNotNull(listInClass);
        assertEquals(26, listInClass.len);
        assertEquals("ENG", listInClass.string_type);
        assertEquals(2, listInClass.repeat);

        assertNotNull(listInClass.list);
        assertEquals("한글", listInClass.list.get(0).with_object_string_type);
        assertEquals("한글ENG", listInClass.list.get(1).with_object_string_type);
    }

    @Test
    public void 메시지핸들러_HEXA_테스트() {
        byte[] key = "1234567890123456".getBytes();
        byte[] iv = new byte[16];

        String result = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandlers(new MessageAesCryptoHandler(key, iv, "UTF-8", MessageAesCryptoHandler.MessageType.HEXA))
                .build()
                .encode(listInClass, len -> len - 4);

        System.out.println(result);

        ListInClass listInClass = TcpMessageConverter.builder()
                .charset("UTF-8")
                .messageHandlers(new MessageAesCryptoHandler(key, iv, "UTF-8", MessageAesCryptoHandler.MessageType.HEXA))
                .build()
                .decode(result, ListInClass.class);

        assertNotNull(listInClass);
        assertEquals(26, listInClass.len);
        assertEquals("ENG", listInClass.string_type);
        assertEquals(2, listInClass.repeat);

        assertNotNull(listInClass.list);
        assertEquals("한글", listInClass.list.get(0).with_object_string_type);
        assertEquals("한글ENG", listInClass.list.get(1).with_object_string_type);
    }

    public static class EncodeSingleClass {
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int len;
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public long long_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public double double_type;
        @Column(len = 20)
        public String string_type;
        @Column(len = 10)
        public EnumType enumType;
    }

    public static enum EnumType {
        VALUE_1, VALUE2;
    }

    public static class EncodeSingleWithObjectClass {
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int len;
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public long long_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public double double_type;
        @Column(len = 20)
        public String string_type;
        @Column(len = 10)
        public WithObject withObject;
    }

    public static class WithObject {
        @Column(len = 10)
        public String with_object_string_type;
    }

    public static class HierachySuperClass {
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int len;
        @Column(len = 4)
        public String string_type;
    }

    public static class HierachySubClass extends HierachySuperClass {
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
        @Column(len = 20)
        public String sub_string_type;
    }

    public static class ListInClass {
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int len;
        @Column(len = 4)
        public String string_type;
        @Column(len = 2, rightJustify = true, padding = "0", repeat = true)
        public int repeat;
        @Column(len = 10)
        public List<WithObject> list;
    }
}