package com.payment.skpay;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.annotation.MsgLenColumn;

import java.util.List;
import java.util.Map;

public class Mock {
    public static class SingleClass {
        public boolean boolean_type;
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int integer_number;
        @Column(len = 4, rightJustify = true, padding = "0")
        public long long_number;
        public double double_number;
        public float float_number;
        @Column(len = 8)
        public String str;
        @Column(len = 8)
        public List<ListElementClass> lists;
        public Map<String, String> map;
    }

    public static class ListElementClass {
        @Column(len = 8)
        public String str = "element";
    }

    public static class SuperClass {
        public boolean super_boolean_type;
        @Column(len = 4, rightJustify = true, padding = "0")
        @MsgLenColumn
        public int super_integer_number;
        @Column(len = 4, rightJustify = true, padding = "0")
        public long super_long_number;
        public double super_double_number;
        public float super_float_number;
        @Column(len = 8)
        public String super_str;
        @Column(len = 8)
        public List<ListElementClass> super_lists;
        public Map<String, String> super_map;
    }

    public static class SubClass extends SuperClass {
        public boolean boolean_type;
        @Column(len = 4, rightJustify = true, padding = "0")
        public int integer_number;
        @Column(len = 4, rightJustify = true, padding = "0")
        public long long_number;
        public double double_number;
        public float float_number;
        @Column(len = 8)
        public String str;
        @Column(len = 8)
        public List<ListElementClass> lists;
        public Map<String, String> map;
    }

    public static class InPOJOFielcClass {
        @Column(len = 0)
        public InnerClass innerClass;
    }

    public static class InnerClass {
        public boolean inner_boolean_type;
        @Column(len = 4)
        @MsgLenColumn
        public int inner_integer_number;
        @Column(len = 4)
        public long inner_long_number;
        public double inner_double_number;
        public float inner_float_number;
        @Column(len = 8)
        public String inner_str;
        @Column(len = 8)
        public List<ListElementClass> inner_lists;
        public Map<String, String> inner_map;
    }
}
