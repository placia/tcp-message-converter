package com.payment.skpay.type;

import com.payment.skpay.type.encoder.ListEncoder;
import com.payment.skpay.type.encoder.ObjectEncoder;
import com.payment.skpay.type.encoder.PrimitivesEncoder;
import com.payment.skpay.type.encoder.TypeEncoder;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;

public class TypeConsiderTest {

    @Before
    public void setUp() throws Exception {
    }

    private Field getField(String fieldName) throws NoSuchFieldException {
        return BinderChooseClass.class.getDeclaredField(fieldName);
    }

    @Test
    public void get_TypeEncoder() throws NoSuchFieldException {
        TypeConsider typeConsider = TypeConsider.getInstance();

        TypeEncoder booleanEncoder = typeConsider.getTypeEncoder(getField("boolean_type"));
        assertEquals(PrimitivesEncoder.BOOLEAN_VALUE_ENCODER.getClass(), booleanEncoder.getClass());

        TypeEncoder integerEncoder = typeConsider.getTypeEncoder(getField("integer_type"));
        assertEquals(PrimitivesEncoder.INTEGER_VALUE_ENCODER.getClass(), integerEncoder.getClass());

        TypeEncoder longEncoder = typeConsider.getTypeEncoder(getField("long_type"));
        assertEquals(PrimitivesEncoder.LONG_VALUE_ENCODER.getClass(), longEncoder.getClass());

        TypeEncoder doubleEncoder = typeConsider.getTypeEncoder(getField("double_type"));
        assertEquals(PrimitivesEncoder.DOUBLE_VALUE_ENCODER.getClass(), doubleEncoder.getClass());

        TypeEncoder stringEncoder = typeConsider.getTypeEncoder(getField("string_type"));
        assertEquals(PrimitivesEncoder.STRING_VALUE_ENCODER.getClass(), stringEncoder.getClass());

        TypeEncoder listEncoder = typeConsider.getTypeEncoder(getField("list_type"));
        assertEquals(ListEncoder.class, listEncoder.getClass());

        TypeEncoder objectEncoder = typeConsider.getTypeEncoder(getField("object_type"));
        assertEquals(ObjectEncoder.class, objectEncoder.getClass());
    }

    public static class BinderChooseClass {
        public boolean boolean_type;
        public int integer_type;
        public long long_type;
        public float float_type;
        public double double_type;
        public String string_type;
        public List<ElementClass> list_type;
        public ElementClass object_type;
    }

    public static class ElementClass {
        public boolean boolean_type;
        public int integer_type;
        public long long_type;
        public float float_type;
        public double double_type;
        public String string_type;
    }
}