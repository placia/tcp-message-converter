package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.exception.ColumnEncodeException;
import com.payment.skpay.type.resolver.ListTypeResolver;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ListEncoderTest {
    String charset = "UTF-8";
    ListMockClass listMockClass;

    @Before
    public void setUp() throws Exception {
        ListElementMockClass elementMockClass_1 = new ListElementMockClass();
        elementMockClass_1.string_type = "ENG";
        elementMockClass_1.integer_type = 1000;

        ListElementMockClass elementMockClass_2 = new ListElementMockClass();
        elementMockClass_2.string_type = "한글";
        elementMockClass_2.integer_type = 2000;

        listMockClass = new ListMockClass();

        listMockClass.objectElementList = new ArrayList<>();
        listMockClass.objectElementList.add(elementMockClass_1);
        listMockClass.objectElementList.add(elementMockClass_2);

        listMockClass.stringElementList = new ArrayList<>();
        listMockClass.stringElementList.add("ENG");
        listMockClass.stringElementList.add("한글");
    }

    private Field getField(String fieldName) throws NoSuchFieldException {
        return ListMockClass.class.getDeclaredField(fieldName);
    }

    @Test
    public void 인코딩_테스트() throws NoSuchFieldException {
        ListTypeResolver resolver = new ListTypeResolver();
        Type elementType = resolver.elementType(getField("objectElementList").getGenericType());

        ListEncoder encoder = new ListEncoder(elementType);
        String result = encoder.encode(getField("objectElementList"), charset, listMockClass);

        assertEquals("ENG       0000001000한글    0000002000", result);
    }

    @Test(expected = ColumnEncodeException.class)
    public void 지원하지않는_리스트항목_테스트() throws NoSuchFieldException {
        ListTypeResolver resolver = new ListTypeResolver();
        Type elementType = resolver.elementType(getField("stringElementList").getGenericType());

        ListEncoder encoder = new ListEncoder(elementType);
        String result = encoder.encode(getField("stringElementList"), charset, listMockClass);
    }

    @Test
    public void 디코딩_테스트() throws NoSuchFieldException, UnsupportedEncodingException {
        String message = "ENG       0000001000한글    0000002000";
        byte[] bytes = message.getBytes(charset);

        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.flip();

        ListTypeResolver resolver = new ListTypeResolver();
        Type elementType = resolver.elementType(getField("objectElementList").getGenericType());

        ListEncoder encoder = new ListEncoder(elementType);
        List<ListElementMockClass> result = encoder.decode(getField("objectElementList"), charset, 2, byteBuffer);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("ENG", result.get(0).string_type);
        assertEquals(1000, result.get(0).integer_type);
        assertEquals("한글", result.get(1).string_type);
        assertEquals(2000, result.get(1).integer_type);
    }

    public static class ListMockClass {
        @Column(len = 20) //리스트인 경우 length 계산에 이용되지만 실제적인 메시지 길이는 리스트 오브젝트의 column에 선언된 길이로 결정됨.(리스트 1개가 존재하는 경우의 길이로 표시)
        public List<ListElementMockClass> objectElementList;
        @Column(len = 20)
        public List<String> stringElementList;
    }

    public static class ListElementMockClass {
        @Column(len = 10)
        public String string_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
    }
}