package com.payment.skpay.type.encoder;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ListEncoderHelperTest {

    @Test
    public void supports() throws NoSuchFieldException {
        ListEncoderHelper encoderHelper = new ListEncoderHelper();
        boolean is_support = encoderHelper.supports(ListMockClass.class.getDeclaredField("objectElementList").getGenericType());
        assertEquals(true, is_support);

        is_support = encoderHelper.supports(ListMockClass.class.getDeclaredField("stringElementList").getGenericType());
        assertEquals(true, is_support);
    }

    public static class ListMockClass {
        public List<ListElementMockClass> objectElementList;
        public List<String> stringElementList;
    }

    public static class ListElementMockClass {
        public String string_type;
    }
}