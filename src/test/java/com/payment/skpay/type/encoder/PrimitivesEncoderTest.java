package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;

public class PrimitivesEncoderTest {
    String charset = "UTF-8";

    @Before
    public void setUp() throws Exception {
    }

    public Field getField(String fieldName) throws NoSuchFieldException {
        return PrimitiveMockClass.class.getDeclaredField(fieldName);
    }

    @Test
    public void 프리미트브타입_인코딩_테스트() throws NoSuchFieldException {
        PrimitiveMockClass mockClass = new PrimitiveMockClass();

        String boolean_value = PrimitivesEncoder.BOOLEAN_VALUE_ENCODER.encode(getField("boolean_type"), charset, mockClass);
        assertEquals("true      ", boolean_value);

        String integer_value = PrimitivesEncoder.INTEGER_VALUE_ENCODER.encode(getField("integer_type"), charset, mockClass);
        assertEquals("0000001000", integer_value);

        String long_value = PrimitivesEncoder.LONG_VALUE_ENCODER.encode(getField("long_type"), charset, mockClass);
        assertEquals("0000001000", long_value);

        String float_value = PrimitivesEncoder.DOUBLE_VALUE_ENCODER.encode(getField("float_type"), charset, mockClass);
        assertEquals("00001000.0", float_value);

        String double_value = PrimitivesEncoder.DOUBLE_VALUE_ENCODER.encode(getField("double_type"), charset, mockClass);
        assertEquals("00001000.0", double_value);

        String string_value = PrimitivesEncoder.STRING_VALUE_ENCODER.encode(getField("string_type"), charset, mockClass);
        assertEquals("ENG       ", string_value);

        mockClass.string_type = "한글";
        string_value = PrimitivesEncoder.STRING_VALUE_ENCODER.encode(getField("string_type"), charset, mockClass);
        assertEquals("한글    ", string_value);
    }

    @Test
    public void 프리미티브타입_디코딩_테스트() throws UnsupportedEncodingException, NoSuchFieldException {
        String mesage = "true      0000001000000000100000001000.000001000.0ENG                 ";
        byte[] bytes = mesage.getBytes(charset);

        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.flip();

        boolean boolean_value = PrimitivesEncoder.BOOLEAN_VALUE_ENCODER.decode(getField("boolean_type"), charset, 0, byteBuffer);
        assertEquals(true, boolean_value);

        int integer_value = PrimitivesEncoder.INTEGER_VALUE_ENCODER.decode(getField("integer_type"), charset, 0, byteBuffer);
        assertEquals(1000, integer_value);

        long long_value = PrimitivesEncoder.LONG_VALUE_ENCODER.decode(getField("long_type"), charset, 0, byteBuffer);
        assertEquals(1000, long_value);

        double float_value = PrimitivesEncoder.DOUBLE_VALUE_ENCODER.decode(getField("float_type"), charset, 0, byteBuffer);
        assertEquals(1000.0f, float_value, 0);

        double double_value = PrimitivesEncoder.DOUBLE_VALUE_ENCODER.decode(getField("double_type"), charset, 0, byteBuffer);
        assertEquals(1000.0, double_value, 0);

        String string_value = PrimitivesEncoder.STRING_VALUE_ENCODER.decode(getField("string_type"), charset, 0, byteBuffer);
        assertEquals("ENG", string_value);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void 바이트배열_지원안함_오류_테스트() throws NoSuchFieldException {
        PrimitiveMockClass mockClass = new PrimitiveMockClass();

        String bytes_array_value = PrimitivesEncoder.BYTEARRAY_VALUE_ENCODER.encode(getField("bytes_array_type"), charset, mockClass);
    }

    public static class PrimitiveMockClass {
        @Column(len = 10)
        public boolean boolean_type = true;
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type = 1000;
        @Column(len = 10, rightJustify = true, padding = "0")
        public long long_type = 1000L;
        @Column(len = 10, rightJustify = true, padding = "0")
        public float float_type = 1000.0F;
        @Column(len = 10, rightJustify = true, padding = "0")
        public double double_type = 1000.0;
        @Column(len = 10)
        public String string_type = "ENG";
        @Column(len = 10)
        public byte[] bytes_array_type;
    }
}