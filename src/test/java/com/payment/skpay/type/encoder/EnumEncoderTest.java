package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import static org.junit.Assert.*;

public class EnumEncoderTest {

    @Test
    public void encode() throws NoSuchFieldException {
        EnumTestClass enumTestClass = new EnumTestClass();
        enumTestClass.enumTest = EnumTest.VALUE_1;
        enumTestClass.stringEnumTest = StringEnumTest.STRING_1;

        EnumEncoder enumEncoder = new EnumEncoder(null);
        String result1 = enumEncoder.encode(EnumTestClass.class.getDeclaredField("enumTest"), "UTF-8", enumTestClass);
        assertEquals("VALUE_1   ", result1);

        String result2 = enumEncoder.encode(EnumTestClass.class.getDeclaredField("stringEnumTest"), "UTF-8", enumTestClass);
        assertEquals("A         ", result2);
    }

    @Test
    public void decode() throws UnsupportedEncodingException, NoSuchFieldException {
        String message = "VALUE_1   A         ";
        byte[] bytes = message.getBytes("UTF-8");

        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.flip();

        EnumEncoder enumEncoder = new EnumEncoder(null);
        EnumTest enumTest = (EnumTest) enumEncoder.decode(EnumTestClass.class.getDeclaredField("enumTest"), "UTF-8", 0, byteBuffer);
        assertEquals(EnumTest.VALUE_1, enumTest);

        StringEnumTest stringEnumTest = (StringEnumTest) enumEncoder.decode(EnumTestClass.class.getDeclaredField("stringEnumTest"), "UTF-8", 0, byteBuffer);
        assertEquals(StringEnumTest.STRING_1, stringEnumTest);
    }

    public static class EnumTestClass {
        @Column(len = 10)
        public EnumTest enumTest;
        @Column(len = 10)
        public StringEnumTest stringEnumTest;
    }

    public static enum EnumTest {
        VALUE_1, VALUE_2, VALUE_3;
    }

    public static enum StringEnumTest {
        STRING_1("A"), STRING_2("B"), STRING_3("C");

        private String value;
        private StringEnumTest(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}