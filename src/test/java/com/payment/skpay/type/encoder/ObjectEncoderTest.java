package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ObjectEncoderTest {
    String charset = "UTF-8";

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void 인코딩_테스트() throws NoSuchFieldException {
        Field field = ObjectFieldClass.class.getDeclaredField("elementClass");
        ObjectEncoder encoder = new ObjectEncoder(field.getType());

        ObjectFieldClass obj = new ObjectFieldClass();
        obj.elementClass = new ElementClass();
        obj.elementClass.boolean_type = true;
        obj.elementClass.integer_type = 1000;
        obj.elementClass.long_type = 2000;
        obj.elementClass.float_type = 3000.0f;
        obj.elementClass.double_type = 4000.0;
        obj.elementClass.string_type = "ENG";
        obj.elementClass.elementSubClass = new ElementSubClass();
        obj.elementClass.elementSubClass.integer_type = 5000;

        String result = encoder.encode(field, "UTF-8", obj);
        System.out.println(String.format("[%s]", result));
        assertEquals("true      0000001000000000200000003000.000004000.0ENG       0000005000", result);
    }

    @Test
    public void 디코딩_테스트() throws UnsupportedEncodingException, NoSuchFieldException {
        Field field = ObjectFieldClass.class.getDeclaredField("elementClass");
        String mesage = "true      0000001000000000200000003000.000004000.0ENG       0000005000";
        byte[] bytes = mesage.getBytes(charset);

        ByteBuffer byteBuffer = ByteBuffer.allocate(bytes.length);
        byteBuffer.put(bytes);
        byteBuffer.flip();

        ObjectEncoder<ElementClass> encoder = new ObjectEncoder(field.getType());
        ElementClass obj = encoder.decode(field, charset, 0, byteBuffer);

        assertEquals(true, obj.boolean_type);
        assertEquals(1000, obj.integer_type);
        assertEquals(2000, obj.long_type);
        assertEquals(3000, obj.float_type, 0);
        assertEquals(4000, obj.double_type, 0);
        assertEquals("ENG", obj.string_type);

        assertNotNull(obj.elementSubClass);
        assertEquals(5000, obj.elementSubClass.integer_type);
    }

    public static class ObjectFieldClass {
        @Column(len = 70)
        public ElementClass elementClass;
    }

    public static class ElementClass {
        @Column(len = 10)
        public boolean boolean_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public long long_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public double float_type;
        @Column(len = 10, rightJustify = true, padding = "0")
        public double double_type;
        @Column(len = 10)
        public String string_type;
        @Column(len = 10)
        public ElementSubClass elementSubClass;
    }

    public static class ElementSubClass {
        @Column(len = 10, rightJustify = true, padding = "0")
        public int integer_type;
    }
}