package com.payment.skpay.type.encoder;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Type;

import static org.junit.Assert.assertEquals;

public class PrimitivesEncoderHelperTest {

    @Before
    public void setUp() throws Exception {
    }

    public Type getType(String fieldName) throws NoSuchFieldException {
        return PrimitiveMockClass.class.getDeclaredField(fieldName).getType();
    }

    @Test
    public void support_test() throws NoSuchFieldException {
        boolean boolean_support = PrimitivesEncoderHelper.BOOLEAN_ENCODER_HELPER.supports(getType("boolean_type"));
        assertEquals(true, boolean_support);

        boolean integer_support = PrimitivesEncoderHelper.INTEGER_ENCODER_HELPER.supports(getType("integer_type"));
        assertEquals(true, integer_support);

        boolean long_support = PrimitivesEncoderHelper.LONG_ENCODER_HELPER.supports(getType("long_type"));
        assertEquals(true, long_support);

        boolean float_support = PrimitivesEncoderHelper.DOUBLE_ENCODER_HELPER.supports(getType("float_type"));
        assertEquals(true, float_support);

        boolean double_support = PrimitivesEncoderHelper.DOUBLE_ENCODER_HELPER.supports(getType("double_type"));
        assertEquals(true, double_support);

        boolean string_support = PrimitivesEncoderHelper.STRING_ENCODER_HELPER.supports(getType("string_type"));
        assertEquals(true, string_support);

        boolean bytes_array_support = PrimitivesEncoderHelper.BYTEARRAY_ENCODER_HELPER.supports(getType("bytes_array_type"));
        assertEquals(false, bytes_array_support);
    }

    @Test
    public void getBinder_test() {
        TypeEncoder boolean_TypeEncoder = PrimitivesEncoderHelper.BOOLEAN_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.BOOLEAN_VALUE_ENCODER, boolean_TypeEncoder);

        TypeEncoder integer_TypeEncoder = PrimitivesEncoderHelper.INTEGER_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.INTEGER_VALUE_ENCODER, integer_TypeEncoder);

        TypeEncoder long_TypeEncoder = PrimitivesEncoderHelper.LONG_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.LONG_VALUE_ENCODER, long_TypeEncoder);

        TypeEncoder double_TypeEncoder = PrimitivesEncoderHelper.DOUBLE_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.DOUBLE_VALUE_ENCODER, double_TypeEncoder);

        TypeEncoder string_TypeEncoder = PrimitivesEncoderHelper.STRING_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.STRING_VALUE_ENCODER, string_TypeEncoder);

        TypeEncoder bytearray_TypeEncoder = PrimitivesEncoderHelper.BYTEARRAY_ENCODER_HELPER.getEncoderBy(null);
        assertEquals(PrimitivesEncoder.BYTEARRAY_VALUE_ENCODER, bytearray_TypeEncoder);
    }

    public static class PrimitiveMockClass {
        private boolean boolean_type;
        private int integer_type;
        private long long_type;
        private float float_type;
        private double double_type;
        private String string_type;
        private byte[] bytes_array_type;
    }
}