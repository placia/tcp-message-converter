package com.payment.skpay.type;

import com.payment.skpay.type.encoder.ListEncoder;
import com.payment.skpay.type.encoder.ObjectEncoder;
import com.payment.skpay.type.encoder.PrimitivesEncoder;
import com.payment.skpay.type.encoder.TypeEncoder;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TypeEncoderHelperTest {

    @Before
    public void setUp() throws Exception {
    }

    private Field getField(String fieldName) throws NoSuchFieldException {
        return BinderChooseClass.class.getDeclaredField(fieldName);
    }

    @Test
    public void get_encoder_test() throws NoSuchFieldException {
        TypeEncoderManager encoderManager = new TypeEncoderManager();

        TypeEncoder booleanEncoder = encoderManager.getEncoderBy(getField("boolean_type").getType());
        assertEquals(PrimitivesEncoder.BOOLEAN_VALUE_ENCODER.getClass(), booleanEncoder.getClass());

        TypeEncoder integerEncoder = encoderManager.getEncoderBy(getField("integer_type").getType());
        assertEquals(PrimitivesEncoder.INTEGER_VALUE_ENCODER.getClass(), integerEncoder.getClass());

        TypeEncoder longEncoder = encoderManager.getEncoderBy(getField("long_type").getType());
        assertEquals(PrimitivesEncoder.LONG_VALUE_ENCODER.getClass(), longEncoder.getClass());

        TypeEncoder doubleEncoder = encoderManager.getEncoderBy(getField("double_type").getType());
        assertEquals(PrimitivesEncoder.DOUBLE_VALUE_ENCODER.getClass(), doubleEncoder.getClass());

        TypeEncoder stringEncoder = encoderManager.getEncoderBy(getField("string_type").getType());
        assertEquals(PrimitivesEncoder.STRING_VALUE_ENCODER.getClass(), stringEncoder.getClass());

        TypeEncoder listEncoder = encoderManager.getEncoderBy(getField("list_type").getGenericType());
        assertEquals(ListEncoder.class, listEncoder.getClass());

        TypeEncoder objectEncoder = encoderManager.getEncoderBy(getField("object_type").getType());
        assertEquals(ObjectEncoder.class, objectEncoder.getClass());
    }

    public static class BinderChooseClass {
        public boolean boolean_type;
        public int integer_type;
        public long long_type;
        public float float_type;
        public double double_type;
        public String string_type;
        public List<ElementClass> list_type;
        public ElementClass object_type;
    }

    public static class ElementClass {
        public boolean boolean_type;
        public int integer_type;
        public long long_type;
        public float float_type;
        public double double_type;
        public String string_type;
    }
}