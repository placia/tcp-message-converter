package com.payment.skpay;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.*;

public class FieldCacheTest {
    @Test
    public void 싱글클래스() throws NoSuchFieldException {
        Class<?> mock = Mock.SingleClass.class;

        FieldCache fieldCache = FieldCache.getInstance();
        fieldCache.get(mock);

        List<Field> fields = fieldCache.get(mock);

        assertEquals(8, fields.size());
        assertEquals(true, fields.contains(mock.getDeclaredField("boolean_type")));
        assertEquals(true, fields.contains(mock.getDeclaredField("integer_number")));
        assertEquals(true, fields.contains(mock.getDeclaredField("long_number")));
        assertEquals(true, fields.contains(mock.getDeclaredField("double_number")));
        assertEquals(true, fields.contains(mock.getDeclaredField("float_number")));
        assertEquals(true, fields.contains(mock.getDeclaredField("str")));
        assertEquals(true, fields.contains(mock.getDeclaredField("lists")));
        assertEquals(true, fields.contains(mock.getDeclaredField("map")));
    }

    @Test
    public void 상속클래스() throws NoSuchFieldException {
        Class<?> mock_sub = Mock.SubClass.class;
        Class<?> mock_super = Mock.SuperClass.class;

        FieldCache fieldCache = FieldCache.getInstance();
        fieldCache.get(mock_sub);

        List<Field> fields = fieldCache.get(mock_sub);

        assertEquals(16, fields.size());
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_boolean_type")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_integer_number")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_long_number")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_double_number")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_float_number")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_str")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_lists")));
        assertEquals(true, fields.contains(mock_super.getDeclaredField("super_map")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("boolean_type")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("integer_number")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("long_number")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("double_number")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("float_number")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("str")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("lists")));
        assertEquals(true, fields.contains(mock_sub.getDeclaredField("map")));
    }

    @Test
    public void 필드내_POJO() throws NoSuchFieldException {
        Class<?> mock_inPOJO = Mock.InPOJOFielcClass.class;
        Class<?> mock_inner = Mock.InnerClass.class;

        FieldCache fieldCache = FieldCache.getInstance();
        fieldCache.get(mock_inPOJO);

        List<Field> fields = fieldCache.get(mock_inPOJO);
        assertEquals(1, fields.size());
        assertEquals(true, fields.contains(mock_inPOJO.getDeclaredField("innerClass")));

        fields = fieldCache.get(mock_inner);
        assertEquals(8, fields.size());
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_boolean_type")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_integer_number")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_long_number")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_double_number")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_float_number")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_str")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_lists")));
        assertEquals(true, fields.contains(mock_inner.getDeclaredField("inner_map")));
    }
}