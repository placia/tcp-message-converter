package com.payment.skpay.exception;

public class TypeException extends RuntimeException {
    public TypeException(String message) {
        super(message);
    }

    public TypeException(Throwable throwable) {
        super(throwable);
    }
}
