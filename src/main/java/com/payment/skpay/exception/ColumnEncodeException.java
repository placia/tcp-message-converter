package com.payment.skpay.exception;

public class ColumnEncodeException extends RuntimeException {
    public ColumnEncodeException() {
        super();
    }

    public ColumnEncodeException(Throwable throwable) {
        super(throwable);
    }

    public ColumnEncodeException(String message) {
        super(message);
    }

    public ColumnEncodeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
