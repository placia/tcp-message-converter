package com.payment.skpay.exception;

public class MessageHandlerException extends RuntimeException {
    public MessageHandlerException(Throwable throwable) {
        super(throwable);
    }

    public MessageHandlerException(String message) {
        super(message);
    }

    public MessageHandlerException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
