package com.payment.skpay;

import com.payment.skpay.handler.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.*;
import java.util.function.Function;

public class TcpMessageConverter {
    private static Logger log = LoggerFactory.getLogger(TcpMessageConverter.class);

    private Encoder encoder;
    private Decoder decoder;

    private List<MessageHandler> messageHandlers = new ArrayList<>();

    private TcpMessageConverter() {}

    public static TcpMessageConverterBuilder builder() {
        return new TcpMessageConverterBuilder();
    }

    public String encode(Object targetObj) {
        return encode(targetObj, Function.identity());
    }

    /**
     *
     * @param targetObj
     * @param strategy  셋팅된 Object에 대한 전체길이를 가져온 후 strategy를 통해 보정한다. 일반적으로 전문 lenght는 헤더를 제외한 바디 length 또는 전문 length 필드만 제외한 필드 총 length로 셋팅함. 헤더 length 또는 전문 length 필드를 제외하기 위해 고정된 값으로 빼주는 function을 파라미터로 받는다.
     * @return
     */
    public String encode(Object targetObj, Function<Integer, Integer> strategy) {
        int messageTotalLength = encoder.sizeOf(targetObj);
        int messageLengthForField = strategy.apply(messageTotalLength);
        encoder.setLenValue(messageLengthForField, targetObj);

        String result = encoder.encode(targetObj);
        log.debug("encode message: {}", result);

        for (MessageHandler handler : messageHandlers) {
            result = handler.executeAfterEncode(result);
        }

        log.debug("after handler message: {}", result);
        return result;
    }

    public <T> T decode(String targetMsg, Class<T> classOfResult) {
        log.debug("before handler message: {}", targetMsg);

        List<MessageHandler> reverseMessageHandlers = new ArrayList<>(messageHandlers);
        Collections.reverse(reverseMessageHandlers);
        for (MessageHandler handler : reverseMessageHandlers) {
            targetMsg = handler.executeBeforeDecode(targetMsg);
        }

        log.debug("after handler message: {}", targetMsg);
        return decoder.decode(targetMsg, classOfResult);
    }

    /**
     * 전문생성 후속처리 또는 전문파싱 전처리 등을 처리할 핸들러를 등록함.
     * @return
     */
    public TcpMessageConverter addMessageHandler(MessageHandler messageHandler) {
        messageHandlers.add(messageHandler);
        return this;
    }

    public static class TcpMessageConverterBuilder {
        private String charset;
        private MessageHandler[] messageHandlers;

        public TcpMessageConverterBuilder charset(String charset) {
            this.charset = charset;
            return this;
        }

        public TcpMessageConverterBuilder messageHandlers(MessageHandler... messageHandlers) {
            this.messageHandlers = messageHandlers;
            return this;
        }

        public TcpMessageConverter build() {
            if (charset == null) {
                charset = Charset.defaultCharset().name();
            }

            TcpMessageConverter messageConverter = new TcpMessageConverter();
            messageConverter.encoder = new MessageEncoder(charset);
            messageConverter.decoder = new MessageDecoder(charset);
            if (messageHandlers != null)
                messageConverter.messageHandlers = Arrays.asList(messageHandlers);

            return messageConverter;
        }
    }
}
