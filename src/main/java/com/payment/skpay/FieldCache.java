package com.payment.skpay;

import com.payment.skpay.type.TypeConsider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class FieldCache {
    private Logger log = LoggerFactory.getLogger(FieldCache.class);

    private TypeConsider typeConsider = TypeConsider.getInstance();
    private static Map<Class<?>, List<Field>> cache = new ConcurrentHashMap<>();

    private FieldCache() { }

    private static class Singleton {
        private static final FieldCache instance = new FieldCache();
    }

    public static FieldCache getInstance () {
        return Singleton.instance;
    }

    public <T> List<Field> get(Class<T> classOfT) {
        if (!cache.containsKey(classOfT)) {
            cache.put(classOfT, disassemble(classOfT));
            log.debug("class caching: {}", classOfT.getSimpleName());
        }

        return cache.get(classOfT);
    }

    private <T> List<Field> disassemble(Class<T> classOfT) {
        //super 클래스의 필드를 캐싱, 상속에 따라 java.lang.Object 타입이 될 때 까지 재귀처리함.
        List<Field> superClassFields = Optional.ofNullable(classOfT.getSuperclass())
                .map(s -> disassemble(s))
                .orElseGet(() -> new ArrayList<>());

        List<Field> targetClassFields = Arrays.asList(classOfT.getDeclaredFields());

        List<Field> result = new ArrayList<>();
        result.addAll(superClassFields);
        result.addAll(targetClassFields);

        return result;
    }
}
