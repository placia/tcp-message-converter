package com.payment.skpay.type;

import com.payment.skpay.type.encoder.*;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TypeEncoderManager {
    private static List<TypeEncoderHelper> typeEncoderHelpers;

    static {
        //맵, byte array 필드는 지원하지 않는다.
        if (typeEncoderHelpers == null) {
            typeEncoderHelpers = new ArrayList<>();
            typeEncoderHelpers.add(PrimitivesEncoderHelper.BOOLEAN_ENCODER_HELPER);
            typeEncoderHelpers.add(PrimitivesEncoderHelper.INTEGER_ENCODER_HELPER);
            typeEncoderHelpers.add(PrimitivesEncoderHelper.LONG_ENCODER_HELPER);
            typeEncoderHelpers.add(PrimitivesEncoderHelper.DOUBLE_ENCODER_HELPER);
            typeEncoderHelpers.add(PrimitivesEncoderHelper.STRING_ENCODER_HELPER);
            typeEncoderHelpers.add(new EnumEncoderHelper());
            typeEncoderHelpers.add(new ListEncoderHelper());
            typeEncoderHelpers.add(new ObjectEncoderHelper());
        }
    }

    public TypeEncoder getEncoderBy(Type type) {
        for (TypeEncoderHelper typeEncoderHelper : typeEncoderHelpers) {
            if (typeEncoderHelper.supports(type)) {
                return typeEncoderHelper.getEncoderBy(type);
            }
        }

        throw new IllegalArgumentException("no managed type: " + type.getTypeName());
    }
}
