package com.payment.skpay.type.resolver;

import com.payment.skpay.exception.TypeException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MapTypeResolver implements TypeResolver {
    public Type[] keyValueType(Type type) {
        Type[] keyValueType = new Type[2];
        if (!(type instanceof ParameterizedType))
            throw new TypeException("parameter 'type' have to be ParameterizedType");

        ParameterizedType parameterizedType = (ParameterizedType) type;
        Type rawType = parameterizedType.getRawType();
        if (!Map.class.isAssignableFrom((Class<?>) rawType))
            throw new TypeException("It's not map type: " + rawType);

        keyValueType[0] = parameterizedType.getActualTypeArguments()[0];
        keyValueType[1] = parameterizedType.getActualTypeArguments()[1];

        return keyValueType;
    }

    public <T> T create(Type type) {
        if (!(type instanceof ParameterizedType))
            throw new TypeException("parameter 'type' have to be ParameterizedType");

        ParameterizedType parameterizedType = (ParameterizedType) type;
        Type rawType = parameterizedType.getRawType();
        if (!Map.class.isAssignableFrom((Class<?>) rawType))
            throw new TypeException("It's not sort of Map: " + rawType);

        Class<?> clazz = (Class<?>) rawType;
        if (Map.class.isAssignableFrom(clazz)) {
            if (ConcurrentMap.class.isAssignableFrom(clazz)) {
                return (T) new ConcurrentHashMap<Object, Object>();
            } else if (LinkedHashMap.class.isAssignableFrom(clazz)) {
                return (T) new LinkedHashMap<Object, Object>();
            } else {
                return (T) new HashMap<Object, Object>();
            }
        }

        return null;
    }
}
