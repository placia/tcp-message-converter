package com.payment.skpay.type.resolver;

import com.payment.skpay.exception.TypeException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

public class ListTypeResolver implements TypeResolver {
    public Type elementType(Type type) {
        if (!(type instanceof ParameterizedType))
            throw new TypeException("parameter 'type' have to be ParameterizedType");

        ParameterizedType parameterizedType = (ParameterizedType) type;
        Type rawType = parameterizedType.getRawType();
        if (!Collection.class.isAssignableFrom((Class<?>) rawType))
            throw new TypeException("It's not list type: " + type);

        return parameterizedType.getActualTypeArguments()[0];
    }

    public <T> T create(Type type) {
        if (!(type instanceof ParameterizedType))
            throw new TypeException("parameter 'type' have to be ParameterizedType");

        ParameterizedType parameterizedType = (ParameterizedType) type;
        Type rawType = parameterizedType.getRawType();
        Class<?> clazz = (Class<?>) rawType;

        if (!Collection.class.isAssignableFrom(clazz))
            throw new TypeException("It's not list type: " + type);

        if (SortedSet.class.isAssignableFrom(clazz)) {
            return (T) new TreeSet<Object>();
        } else if (Set.class.isAssignableFrom(clazz)) {
            return (T) new LinkedHashSet<Object>();
        } else {
            return (T) new ArrayList<Object>();
        }
    }
}
