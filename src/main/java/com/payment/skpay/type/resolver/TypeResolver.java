package com.payment.skpay.type.resolver;

import java.lang.reflect.Type;

public interface TypeResolver {
    <T> T create(Type type);
}
