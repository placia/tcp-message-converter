package com.payment.skpay.type.resolver;

import com.payment.skpay.exception.TypeException;

import java.lang.reflect.*;
import java.util.Arrays;

public class ObjectTypeResolver implements TypeResolver {
    public <T> T create(Type type) {
        Class<T> rawType = (Class<T>) type;
        if (!Object.class.isAssignableFrom(rawType)) {
            throw new TypeException("It's not Object: " + type);
        }

        Constructor c = Arrays.stream(rawType.getDeclaredConstructors())
                .filter(constructor -> accessModifiers(constructor.getModifiers()) == Modifier.PUBLIC)
                .filter(constructor -> constructor.getParameterCount() == 0)
                .findFirst()
                .orElseThrow(() -> new TypeException("Object constructor is required public modifier and no arguments.: " + ((Class<T>) type).getSimpleName()))
        ;

        T object = null;
        try {
            c.setAccessible(true);
            object = (T) c.newInstance();
        } catch (Exception e) {
            throw new TypeException(e);
        }

        return object;
    }

//    public Method getFieldSetterMethod(Class<?> clazz, Field field) {
//        return getFieldMethod(clazz, Optional.of(field), "set"+getFirstLetterUppercase(field));
//    }
//
//    public Method getFieldGetterMethod(Class<?> clazz, Field field) {
//        return getFieldMethod(clazz, Optional.empty(), "get"+getFirstLetterUppercase(field));
//    }
//
//    private Method getFieldMethod(Class<?> clazz, Optional<Field> argumentField, String methodName) {
//        Method method = null;
//        try {
//            if (argumentField.isPresent())
//                method = clazz.getDeclaredMethod(methodName, argumentField.get().getType());
//            else
//                method = clazz.getDeclaredMethod(methodName);
//            if (accessModifiers(method.getModifiers()) != Modifier.PUBLIC)
//                throw new IllegalArgumentException(method +" is not public method");
//            method.setAccessible(true);
//        } catch (NoSuchMethodException e) {
//            throw new TypeException(e);
//        }
//
//        return method;
//    }

    private int accessModifiers(int m) {
        return m & (Modifier.PUBLIC | Modifier.PRIVATE | Modifier.PROTECTED);
    }

//    public String getBindingName(Field field) {
//        String fieldName = field.getName();
//        BindingName bindingName = field.getDeclaredAnnotation(BindingName.class);
//        if (Objects.nonNull(bindingName)) {
//            fieldName = bindingName.value();
//        } else {
//            fieldName = getFirstLetterUppercase(field);
//        }
//
//        return fieldName;
//    }

//    public String getFirstLetterUppercase(Field field) {
//        String fieldName = field.getName();
//        if (fieldName.length() > 1) {
//            StringBuilder sb = new StringBuilder();
//            int index = 0;
//            boolean isChangeFirstLetterUpperCase = false;
//            for (; index < fieldName.length(); index++) {
//                char letter = fieldName.charAt(index);
//                if (!isChangeFirstLetterUpperCase && Character.isLetter(letter)) {
//                    break;
//                }
//
//                sb.append(letter);
//            }
//
//            sb.append(Character.toUpperCase(fieldName.charAt(index)));
//            sb.append(fieldName.substring(++index));
//            fieldName = sb.toString();
//        }
//
//        return fieldName;
//    }
}
