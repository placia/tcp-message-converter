package com.payment.skpay.type.encoder;

import com.payment.skpay.FieldCache;
import com.payment.skpay.annotation.Column;
import com.payment.skpay.exception.ColumnEncodeException;
import com.payment.skpay.type.TypeConsider;
import com.payment.skpay.type.resolver.ObjectTypeResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;

public class ObjectEncoder<T> implements TypeEncoder<T> {
    private static Logger log = LoggerFactory.getLogger(ObjectEncoder.class);

    private FieldCache fieldCache;
    private TypeConsider typeConsider = TypeConsider.getInstance();

    public ObjectEncoder(Type type) {
        this.fieldCache = FieldCache.getInstance();
    }

    @Override
    public String encode(Field field, String charset, Object targetObj) {
        StringBuilder message = new StringBuilder();

        try {
            field.setAccessible(true);
            Object fieldObj = field.get(targetObj);

            List<Field> fields = fieldCache.get(field.getType());
            for (Field f : fields) {
                if (!f.isAnnotationPresent(Column.class)) continue;

                f.setAccessible(true);

                //Object내 필드 타입으로 encoding 호출
                TypeEncoder fieldTypeEncoder = typeConsider.getTypeEncoder(f);
                String value = Optional.ofNullable(fieldObj)
                        .map(o -> fieldTypeEncoder.encode(f, charset, o))
                        .orElse("");

                message.append(value);
            }
        } catch (IllegalAccessException e) {
            throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", field.getName()), e);
        }

        return message.toString();
    }

    @Override
    public T decode(Field field, String charset, int count, ByteBuffer targetMsg) {
        ObjectTypeResolver resolver = new ObjectTypeResolver();
        Class<?> classOfResult = field.getType();
        final T result = resolver.create(classOfResult);

        fieldCache.get(classOfResult).stream()
                .filter(f -> f.isAnnotationPresent(Column.class))
                .forEach(f -> {
                    TypeEncoder typeEncoder = typeConsider.getTypeEncoder(f);
                    Optional.ofNullable(typeEncoder.decode(f, charset, 0, targetMsg))
                            .ifPresent(value -> {
                                try {
                                    f.set(result, value);
                                } catch (IllegalAccessException e) {
                                    throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", f.getName()), e);
                                }
                            });
                });

        return result;
    }
}
