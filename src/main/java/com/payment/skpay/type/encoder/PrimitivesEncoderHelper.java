package com.payment.skpay.type.encoder;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrimitivesEncoderHelper {
    public static boolean supports(Type type, List<Class<?>> checkList) {
        return checkList.stream().filter(clazz -> clazz == type)
                .findFirst()
                .isPresent();
    }

    private abstract static class PrimitivesEncoderHelperAdapter<T> implements TypeEncoderHelper<T> {
        private final List<Class<?>> checkList;

        public PrimitivesEncoderHelperAdapter(List<Class<?>> checkList) {
            this.checkList = checkList;
        }

        @Override
        public boolean supports(Type type) {
            return PrimitivesEncoderHelper.supports(type, checkList);
        }
    }

    public abstract static class BooleanEncoderHelper extends PrimitivesEncoderHelperAdapter<Boolean> {
        public BooleanEncoderHelper() {
            super(Arrays.asList(boolean.class, Boolean.class));
        }
    }

    public abstract static class ByteArrayEncoderHelper extends PrimitivesEncoderHelperAdapter<byte[]> {
        public ByteArrayEncoderHelper() {
            super(new ArrayList<>());
        }
    }

    public abstract static class IntegerEncoderHelper extends PrimitivesEncoderHelperAdapter<Integer> {
        public IntegerEncoderHelper() {
            super(Arrays.asList(int.class, Integer.class));
        }
    };

    public abstract static class LongEncoderHelper extends PrimitivesEncoderHelperAdapter<Long> {
        public LongEncoderHelper() {
            super(Arrays.asList(long.class, Long.class));
        }
    };

    public abstract static class DoubleEncoderHelper extends PrimitivesEncoderHelperAdapter<Double> {
        public DoubleEncoderHelper() {
            super(Arrays.asList(float.class, Float.class, double.class, Double.class));
        }
    }

    public abstract static class StringEncoderHelper extends PrimitivesEncoderHelperAdapter<String> {
        public StringEncoderHelper() {
            super(Arrays.asList(String.class));
        }
    };

    public final static TypeEncoderHelper<Boolean> BOOLEAN_ENCODER_HELPER = new BooleanEncoderHelper() {
        @Override
        public TypeEncoder<Boolean> getEncoderBy(Type type) {
            return PrimitivesEncoder.BOOLEAN_VALUE_ENCODER;
        }
    };

    public final static TypeEncoderHelper<byte[]> BYTEARRAY_ENCODER_HELPER = new ByteArrayEncoderHelper() {
        @Override
        public TypeEncoder<byte[]> getEncoderBy(Type type) {
            return PrimitivesEncoder.BYTEARRAY_VALUE_ENCODER;
        }
    };

    public final static TypeEncoderHelper<Integer> INTEGER_ENCODER_HELPER = new IntegerEncoderHelper() {
        @Override
        public TypeEncoder<Integer> getEncoderBy(Type type) {
            return PrimitivesEncoder.INTEGER_VALUE_ENCODER;
        }
    };

    public final static TypeEncoderHelper<Long> LONG_ENCODER_HELPER = new LongEncoderHelper() {
        @Override
        public TypeEncoder<Long> getEncoderBy(Type type) {
            return PrimitivesEncoder.LONG_VALUE_ENCODER;
        }
    };

    public final static TypeEncoderHelper<Double> DOUBLE_ENCODER_HELPER = new DoubleEncoderHelper() {
        @Override
        public TypeEncoder<Double> getEncoderBy(Type type) {
            return PrimitivesEncoder.DOUBLE_VALUE_ENCODER;
        }
    };

    public final static TypeEncoderHelper<String> STRING_ENCODER_HELPER = new StringEncoderHelper() {
        @Override
        public TypeEncoder<String> getEncoderBy(Type type) {
            return PrimitivesEncoder.STRING_VALUE_ENCODER;
        }
    };
}
