package com.payment.skpay.type.encoder;

import com.payment.skpay.type.resolver.ListTypeResolver;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class ListEncoderHelper<E> implements TypeEncoderHelper<List<E>> {
    @Override
    public boolean supports(Type type) {
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();

            return List.class.isAssignableFrom((Class<?>) rawType);
        }

        return false;
    }

    @Override
    public TypeEncoder<List<E>> getEncoderBy(Type type) {
        ListTypeResolver listTypeResolver = new ListTypeResolver();
        //리스트 항목 클래스 타입를 조회
        Type listElementType = listTypeResolver.elementType(type);

        if (!(listElementType instanceof ParameterizedType) && !(listElementType instanceof Class<?>)) {
            throw new IllegalArgumentException("List element type have to be ParameterizedType or Class<?>");
        }

        return new ListEncoder(listElementType);
    }
}
