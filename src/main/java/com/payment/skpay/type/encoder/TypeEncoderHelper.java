package com.payment.skpay.type.encoder;

import java.lang.reflect.Type;

public interface TypeEncoderHelper<T> {
    boolean supports(Type type);
    TypeEncoder<T> getEncoderBy(Type type);
}
