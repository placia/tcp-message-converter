package com.payment.skpay.type.encoder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

public class MapEncoderHelper<V> implements TypeEncoderHelper<Map<String, V>> {
    @Override
    public boolean supports(Type type) {
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type rawType = parameterizedType.getRawType();

            return Map.class.isAssignableFrom((Class<?>) rawType);
        }

        return false;
    }

    @Override
    public TypeEncoder<Map<String, V>> getEncoderBy(Type type) {
        throw new UnsupportedOperationException("Map type is not supported: " + type.getTypeName());
    }
}
