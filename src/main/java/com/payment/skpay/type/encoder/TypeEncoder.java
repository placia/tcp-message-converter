package com.payment.skpay.type.encoder;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface TypeEncoder<T> {
    String encode(Field field, String charset, Object targetObj);
    T decode(Field field, String charset, int count, ByteBuffer targetMsg);

    static boolean isValueLengthOver(int count, int declaredLength, int valueLength) {
        return valueLength > declaredLength * count;
    }

    static String getPadding(final String paddingLetter, int declaredLength, int valueLength) {
        String padding = "";
        if ((declaredLength - valueLength > 0)) {
            padding = Stream.iterate(0, i -> i + 1)
                    .limit(declaredLength - valueLength)
                    .map(i -> paddingLetter)
                    .collect(Collectors.joining());
        }
        return padding;
    }

    static String getFinalValue(boolean rightJustify, String value, String padding) {
        return rightJustify ? padding + value : value + padding;
    }
}
