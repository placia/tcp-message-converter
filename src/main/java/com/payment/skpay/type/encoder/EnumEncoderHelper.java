package com.payment.skpay.type.encoder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class EnumEncoderHelper implements TypeEncoderHelper<Object> {
    @Override
    public boolean supports(Type type) {
        return !(type instanceof ParameterizedType) && ((Class<?>) type).isEnum();
    }

    @Override
    public TypeEncoder getEncoderBy(Type type) {
        return new EnumEncoder(type);
    }
}
