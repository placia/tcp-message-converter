package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.exception.ColumnEncodeException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class EnumEncoder implements TypeEncoder<Object> {
    public EnumEncoder(Type type) {
    }

    @Override
    public String encode(Field field, String charset, Object targetObj) {
        return PrimitivesEncoder.STRING_VALUE_ENCODER.encode(field, charset, targetObj);
    }

    @Override
    public Object decode(Field field, String charset, int count, ByteBuffer targetMsg) {
        Column annotation = field.getAnnotation(Column.class);
        byte[] valueBytes = new byte[annotation.len()];

        field.setAccessible(true);
        targetMsg.get(valueBytes);

        try {
            String value = new String(valueBytes, charset).trim();
            return Arrays.stream(field.getType().getEnumConstants())
                    .filter(e -> e.toString().equals(value))
                    .findFirst()
                    .orElseThrow(() -> new ColumnEncodeException("Unknown enum value: " + value));
        } catch (UnsupportedEncodingException e) {
            new ColumnEncodeException("fail to decode enum type");
        }

        return null;
    }
}
