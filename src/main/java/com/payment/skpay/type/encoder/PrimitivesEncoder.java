package com.payment.skpay.type.encoder;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.exception.ColumnEncodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.Optional;

public class PrimitivesEncoder {
    private static Logger log = LoggerFactory.getLogger(PrimitivesEncoder.class);

    private static String encode(Field field, String charset, Object targetObj) {
        try {
            Column annotation = field.getAnnotation(Column.class);
            field.setAccessible(true);

            String value = Optional.ofNullable(field.get(targetObj))
                    .map(String::valueOf)
                    .map(d -> d.trim())
                    .orElse("");

            //전문은 최종적으로 primitive 타입의 값으로 결정됨. 이곳에서 패딩처리 및 길이 체크
            int valueLength = value.getBytes(charset).length;
            if (TypeEncoder.isValueLengthOver(1, annotation.len(), valueLength)) {
                log.error("invalid value length: [FIELD: {}]:[VALUE: {}:{}] ANNOTATION {}", field.getName(), value, value.length(), annotation.len());
                throw new ColumnEncodeException(String.format("invalid value length: [FIELD: %s]", field.getName()));
            }

            String padding = TypeEncoder.getPadding(annotation.padding(), annotation.len(), valueLength);
            return TypeEncoder.getFinalValue(annotation.rightJustify(), value, padding);
        } catch (IllegalAccessException | UnsupportedEncodingException e) {
            throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", field.getName()), e);
        }
    }

    private static String decode(Field field, String charset, ByteBuffer byteBuffer) {
        Column annotation = field.getAnnotation(Column.class);
        field.setAccessible(true);

        byte[] valueBytes = new byte[annotation.len()];
        byteBuffer.get(valueBytes);

        try {
            return new String(valueBytes, charset).trim();
        } catch (UnsupportedEncodingException e) {
            throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", field.getName()), e);
        }
    }

    public final static TypeEncoder<Boolean> BOOLEAN_VALUE_ENCODER = new TypeEncoder<Boolean>() {

        @Override
        public String encode(Field field, String charset, Object targetObj) {
            return PrimitivesEncoder.encode(field, charset, targetObj);
        }

        @Override
        public Boolean decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            String value = PrimitivesEncoder.decode(field, charset, targetMsg);
            return Boolean.parseBoolean(value);
        }
    };

    public final static TypeEncoder<byte[]> BYTEARRAY_VALUE_ENCODER = new TypeEncoder<byte[]>() {
        @Override
        public String encode(Field field, String charset, Object targetObj) {
            throw new UnsupportedOperationException("byte array is not supported");
        }

        @Override
        public byte[] decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            String value = PrimitivesEncoder.decode(field, charset, targetMsg);
            try {
                return value.getBytes(charset);
            } catch (UnsupportedEncodingException e) {
                throw new ColumnEncodeException(e);
            }
        }
    };

    public final static TypeEncoder<Integer> INTEGER_VALUE_ENCODER = new TypeEncoder<Integer>() {
        @Override
        public String encode(Field field, String charset, Object targetObj) {
            return PrimitivesEncoder.encode(field, charset, targetObj);
        }

        @Override
        public Integer decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            String value = PrimitivesEncoder.decode(field, charset, targetMsg);
            return Integer.parseInt(value.isEmpty() ? "0" : value);
        }
    };

    public final static TypeEncoder<Long> LONG_VALUE_ENCODER = new TypeEncoder<Long>() {
        @Override
        public String encode(Field field, String charset, Object targetObj) {
            return PrimitivesEncoder.encode(field, charset, targetObj);
        }

        @Override
        public Long decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            String value = PrimitivesEncoder.decode(field, charset, targetMsg);
            return Long.parseLong(value.isEmpty() ? "0" : value);
        }
    };

    public final static TypeEncoder<Double> DOUBLE_VALUE_ENCODER = new TypeEncoder<Double>() {
        @Override
        public String encode(Field field, String charset, Object targetObj) {
            return PrimitivesEncoder.encode(field, charset, targetObj);
        }

        @Override
        public Double decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            String value = PrimitivesEncoder.decode(field, charset, targetMsg);
            return Double.parseDouble(value.isEmpty() ? "0.0" : value);
        }
    };

    public final static TypeEncoder<String> STRING_VALUE_ENCODER = new TypeEncoder<String>() {
        @Override
        public String encode(Field field, String charset, Object targetObj) {
            return PrimitivesEncoder.encode(field, charset, targetObj);
        }

        @Override
        public String decode(Field field, String charset, int count, ByteBuffer targetMsg) {
            return PrimitivesEncoder.decode(field, charset, targetMsg);
        }
    };
}
