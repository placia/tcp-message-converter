package com.payment.skpay.type.encoder;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ObjectEncoderHelper<T> implements TypeEncoderHelper<T> {
    @Override
    public boolean supports(Type type) {
        return !(type instanceof ParameterizedType) && Object.class.isAssignableFrom((Class<?>) type);
    }

    @Override
    public TypeEncoder<T> getEncoderBy(Type type) {
        return new ObjectEncoder<>(type);
    }
}
