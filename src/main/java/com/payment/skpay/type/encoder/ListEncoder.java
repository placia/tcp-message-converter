package com.payment.skpay.type.encoder;

import com.payment.skpay.FieldCache;
import com.payment.skpay.exception.ColumnEncodeException;
import com.payment.skpay.type.TypeConsider;
import com.payment.skpay.type.resolver.ListTypeResolver;
import com.payment.skpay.type.resolver.ObjectTypeResolver;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ListEncoder<E> implements TypeEncoder<List<E>> {
    private Type elementType;   //리스트 항목 클래스 타입
    private FieldCache fieldCache;
    private TypeConsider typeConsider;

    public ListEncoder(Type elementType) {
        this.elementType = elementType;
        this.fieldCache = FieldCache.getInstance();
        this.typeConsider = TypeConsider.getInstance();
    }

    @Override
    public String encode(Field field, String charset, Object targetObj) {
        try {
            field.setAccessible(true);

            List<E> list = (List) field.get(targetObj);
            if (list == null || list.size() == 0) return "";

            StringBuilder listMessage = new StringBuilder();
            for (E elementObject : list) {
                validateType();

                listMessage.append(Arrays.stream(elementObject.getClass().getDeclaredFields())
                        .map(f -> {
                            TypeEncoder elementTypeEncoder = typeConsider.getTypeEncoder(f);
                            return elementTypeEncoder.encode(f, charset, elementObject);
                        })
                        .collect(Collectors.joining())
                );
            }

            return listMessage.toString();
        } catch (IllegalAccessException e) {
            throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", field.getName()), e);
        }
    }

    @Override
    public List<E> decode(Field listField, String charset, int count, ByteBuffer targetMsg) {
        ListTypeResolver listResolver = new ListTypeResolver();
        List<E> result = listResolver.create(listField.getGenericType());

        for (int i = 0; i < count; i++) {
            validateType();

            ObjectTypeResolver objectResolver = new ObjectTypeResolver();
            E element = objectResolver.create(elementType);

            Arrays.stream(((Class<?>) elementType).getDeclaredFields())
                    .forEach(f -> {
                        try {
                            TypeEncoder elementTypeEncoder = typeConsider.getTypeEncoder(f);
                            Object value = elementTypeEncoder.decode(f, charset, 0, targetMsg);

                            f.set(element, value);
                        } catch (IllegalAccessException e) {
                            throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", f.getName()), e);
                        }
                    });

            result.add(element);
        }

        return result;
    }

    private void validateType() {
        //리스트는 primitive와 맵, 중첩리스트는 지원하지 않는다.
        if (typeConsider.isPrimitive(elementType)) {
            throw new ColumnEncodeException("List element type must not be primitive type. It is only supported object type.");
        }

        if (typeConsider.isList(elementType) || typeConsider.isMap(elementType)) {
            throw new ColumnEncodeException("List element type must not be list/map type. It is only supported object type.");
        }
    }
}
