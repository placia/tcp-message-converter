package com.payment.skpay.type;

import com.payment.skpay.type.encoder.*;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TypeConsider {
    private static List<Class<?>> primitives = new ArrayList<>();

    private static class Singleton {
        private static final TypeConsider instance = new TypeConsider();
    }

    private TypeConsider() {
        if (primitives.size() == 0) {
            primitives.add(int.class);
            primitives.add(long.class);
            primitives.add(float.class);
            primitives.add(double.class);
            primitives.add(boolean.class);
            primitives.add(String.class);
            primitives.add(byte[].class);
            primitives.add(Integer.class);
            primitives.add(Long.class);
            primitives.add(Float.class);
            primitives.add(Double.class);
            primitives.add(Boolean.class);
        }
    }

    public static TypeConsider getInstance () {
        return TypeConsider.Singleton.instance;
    }

    public TypeEncoder getTypeEncoder(Field field) {
        TypeEncoderManager encoderManager = new TypeEncoderManager();

        //맵, byte array 필드는 지원하지 않는다.
        if (isPrimitive(field)) {
            return encoderManager.getEncoderBy(field.getType());
        } else if (isList(field)) {
            return encoderManager.getEncoderBy(field.getGenericType());
        } else if (isEnum(field)) {
            return encoderManager.getEncoderBy(field.getType());
        } else if (isObject(field)) {
            return encoderManager.getEncoderBy(field.getType());
        } else {
            throw new IllegalArgumentException("no managed field type: " + field.getName());
        }
    }

    public boolean isEnum(Field field) {
        return field.getType().isEnum();
    }

    public boolean isEnum(Type type) {
        EnumEncoderHelper encoderHelper = new EnumEncoderHelper();
        return encoderHelper.supports(type);
    }

    public boolean isObject(Field field) {
        Class<?> type = field.getType();
        return isPrimitive(field) || isList(field) ? false : isObject(type);  //String 타입은 object 타입이므로 체크해야함.
    }

    public boolean isObject(Type type) {
        ObjectEncoderHelper encoderHelper = new ObjectEncoderHelper();
        return encoderHelper.supports(type);
    }

    public boolean isPrimitive(Field field) {
        Class<?> type = field.getType();
        return isPrimitive(type);
    }

    public boolean isPrimitive(Type type) {
        return PrimitivesEncoderHelper.supports(type, primitives);
    }

    public boolean isList(Field field) {
        Type type = field.getGenericType();
        return isList(type);
    }

    public boolean isList(Type type) {
        ListEncoderHelper encoderHelper = new ListEncoderHelper();
        return encoderHelper.supports(type);
    }

    public boolean isMap(Field field) {
        Type type = field.getGenericType();
        return isMap(type);
    }

    public boolean isMap(Type type) {
        MapEncoderHelper encoderHelper = new MapEncoderHelper();
        return encoderHelper.supports(type);
    }
}
