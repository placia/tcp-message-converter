package com.payment.skpay.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {
    int len();
    String padding() default " ";
    boolean rightJustify() default false;
    boolean repeat() default false;
    String comment() default "";
    boolean include() default true;
}
