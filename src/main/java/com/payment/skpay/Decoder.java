package com.payment.skpay;

public interface Decoder {
    <T> T decode(String targetMsg, Class<T> classOfResult);
}
