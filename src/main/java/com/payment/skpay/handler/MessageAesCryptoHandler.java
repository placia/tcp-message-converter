package com.payment.skpay.handler;

import com.payment.skpay.exception.MessageHandlerException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class MessageAesCryptoHandler implements MessageHandler {
    private MessageType messageType;
    private static final String ENC_FORMAT = "AES/CBC/PKCS5Padding";
    private byte[] key;
    private byte[] iv;
    private String charset;

    public MessageAesCryptoHandler(byte[] key, byte[] iv, String charset, MessageType messageType) {
        this.messageType = messageType;
        this.key = key;
        this.iv = iv;
        this.charset = charset;
    }

    @Override
    public String executeAfterEncode(String result) {
        try {
            Cipher cipher = Cipher.getInstance(ENC_FORMAT);
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"),
                    new IvParameterSpec(iv));
            byte[] encryptedMessage = cipher.doFinal(result.getBytes(charset));

            switch (messageType) {
                case BASE64:
                    return Base64.getEncoder().encodeToString(encryptedMessage);
                case HEXA:
                    return bytesToHexString(encryptedMessage);
            }
        } catch (Exception e) {
            throw new MessageHandlerException(e);
        }

        return null;
    }

    @Override
    public String executeBeforeDecode(String targetMsg) {
        byte[] encryptedMessage = null;
        switch (messageType) {
            case BASE64:
                encryptedMessage = Base64.getDecoder().decode(targetMsg);
                break;
            case HEXA:
                encryptedMessage = hexToByteArray(targetMsg);
                break;
        }

        try {
            Cipher cipher = Cipher.getInstance(ENC_FORMAT);
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"),
                    new IvParameterSpec(iv));
            return new String(cipher.doFinal(encryptedMessage), charset);
        } catch (Exception e) {
            throw new MessageHandlerException(e);
        }
    }

    public String bytesToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public byte[] hexToByteArray(String hex) {
        if (hex == null || hex.length() == 0) {
            return null;
        }

        byte[] ba = new byte[hex.length() / 2];
        for (int i = 0; i < ba.length; i++) {
            ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return ba;
    }

    public static enum MessageType {
        BASE64, HEXA
    }
}
