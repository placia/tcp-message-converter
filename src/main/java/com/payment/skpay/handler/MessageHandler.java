package com.payment.skpay.handler;

import com.payment.skpay.exception.MessageHandlerException;

public interface MessageHandler {
    /**
     * 전문생성 후 암호화 등의 후속작업을 실행함.
     *
     * @param result
     * @return
     * @throws MessageHandlerException
     */
    String executeAfterEncode(String result) throws MessageHandlerException;

    /**
     * 전문수신 후 복호화 등의 전처리작업을 수행함.
     * @param targetMsg
     * @return
     * @throws MessageHandlerException
     */
    String executeBeforeDecode(String targetMsg) throws MessageHandlerException;
}
