package com.payment.skpay;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.annotation.MsgLenColumn;
import com.payment.skpay.exception.ColumnEncodeException;
import com.payment.skpay.type.TypeConsider;
import com.payment.skpay.type.encoder.TypeEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

public class MessageEncoder implements Encoder {
    private Logger log = LoggerFactory.getLogger(MessageEncoder.class);

    private String charset = Charset.defaultCharset().name();
    private FieldCache fieldCache = FieldCache.getInstance();
    private TypeConsider typeConsider = TypeConsider.getInstance();

    public MessageEncoder(String charset) {
        this.charset = charset;
    }

    @Override
    public int sizeOf(Object obj) {
        return fieldCache.get(obj.getClass())
                .stream()
                .filter(f -> f.isAnnotationPresent(Column.class))
                .mapToInt(f -> sizeOf(f, obj))
                .sum();
    }

    private int sizeOf(Field f, Object obj) {
        try {
            Column annotation = f.getAnnotation(Column.class);
            f.setAccessible(true);

            if (typeConsider.isPrimitive(f) || typeConsider.isEnum(f)) {
                return annotation.len();
            } else if (typeConsider.isList(f)) {
                return annotation.len() *
                        Optional.ofNullable(((List) f.get(obj))).map(list -> list.size()).orElse(0);
            } else if (typeConsider.isObject(f)) {
                return Optional.ofNullable(f.get(obj)).map(o -> sizeOf(o)).orElse(0);
            } else {
                return 0;
            }
        } catch (IllegalAccessException e) {
            throw new ColumnEncodeException(String.format("invalid access operation: [FIELD: %s]", f.getName()), e);
        }
    }

    @Override
    public void setLenValue(int lenValue, Object obj) {
        if (lenValue < 0) {
            throw new ColumnEncodeException("length value must be greater than 0");
        }

        fieldCache.get(obj.getClass()).stream()
                .filter(f -> f.isAnnotationPresent(MsgLenColumn.class))
                .findFirst()
                .ifPresent(field -> {
                    try {
                        field.setAccessible(true);
                        field.set(obj, lenValue);
                    } catch (IllegalAccessException e) {
                        throw new ColumnEncodeException();
                    }
                });
    }

    @Override
    public String encode(Object obj) {
        return encode(obj, obj.getClass());
    }

    private <T> String encode(Object obj, Class<T> classOfT) {
        StringBuilder message = new StringBuilder();

        int count = 1;
        List<Field> fields = fieldCache.get(classOfT);
        for (Field f : fields) {
            if (!f.isAnnotationPresent(Column.class)) continue;

            try {
                Column annotation = f.getAnnotation(Column.class);
                f.setAccessible(true);

                //각 필드타입별로 전문필드 값을 생성함.
                //패딩 처리는 각 필드타입별 인코딩 처리에서 만들어 리턴함.
                TypeEncoder typeEncoder = typeConsider.getTypeEncoder(f);
                String value = typeEncoder.encode(f, charset, obj);

                int valueLength = value.getBytes(charset).length;
                if (TypeEncoder.isValueLengthOver(count, annotation.len(), valueLength)) {
                    log.error("invalid value length: [FIELD: {}]:[VALUE: {}:{}] ANNOTATION {}", f.getName(), value, value.length(), annotation.len() * count);
                    throw new ColumnEncodeException(String.format("invalid value length: [FIELD: %s]", f.getName()));
                }

                message.append(value);

                //전문내 반복필드는 반복필드가 나오기 전에 반복횟수 필드가 있음.
                //반복횟수 필드로 반복필드의 총 길이를 판단하기 위해 처리함.
                count = resetCount(obj, f, annotation);
            } catch (IllegalAccessException | UnsupportedEncodingException e) {
                throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", f.getName()), e);
            }
        }

        return message.toString();
    }

    private int resetCount(Object obj, Field f, Column annotation) throws IllegalAccessException {
        return annotation.repeat() ? Integer.parseInt(String.valueOf(f.get(obj))) : 1;
    }
}
