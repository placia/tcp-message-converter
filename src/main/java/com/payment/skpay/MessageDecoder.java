package com.payment.skpay;

import com.payment.skpay.annotation.Column;
import com.payment.skpay.exception.ColumnEncodeException;
import com.payment.skpay.type.TypeConsider;
import com.payment.skpay.type.encoder.TypeEncoder;
import com.payment.skpay.type.resolver.ObjectTypeResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

public class MessageDecoder implements Decoder {
    private static Logger log = LoggerFactory.getLogger(MessageDecoder.class);

    private String charset = Charset.defaultCharset().name();
    private FieldCache fieldCache = FieldCache.getInstance();
    private TypeConsider typeConsider = TypeConsider.getInstance();

    public MessageDecoder(String charset) {
        this.charset = charset;
    }

    @Override
    public <T> T decode(String targetMsg, Class<T> classOfResult) {
        if (targetMsg == null || targetMsg.length() == 0) {
            throw new ColumnEncodeException("Message is empty");
        }

        ObjectTypeResolver resolver = new ObjectTypeResolver();
        T result = resolver.create(classOfResult);

        List<Field> fields = fieldCache.get(classOfResult);

        ByteBuffer byteBuffer = null;
        try {
            byte[] bytes = targetMsg.getBytes(charset);
            byteBuffer = ByteBuffer.allocate(bytes.length);
            byteBuffer.put(bytes);
            byteBuffer.flip();
        } catch (UnsupportedEncodingException e) {
            throw new ColumnEncodeException("fail to convert message to ByteBuffer");
        }

        int count = 1;
        for (Field f : fields) {
            if (!f.isAnnotationPresent(Column.class)) continue;
            if (!byteBuffer.hasRemaining()) continue;

            try {
                Column annotation = f.getAnnotation(Column.class);
                f.setAccessible(true);

                Object value = null;
                if (annotation.include()) {
                    TypeEncoder typeEncoder = typeConsider.getTypeEncoder(f);
                    value = typeEncoder.decode(f, charset, count, byteBuffer);

                    if (Objects.isNull(value)) continue;
                    f.set(result, value);
                } else {
                    value = f.get(result);
                }

                log.debug("{} [{}]", annotation.comment(), value);

                count = resetCount(value, f, annotation);
            } catch (IllegalAccessException e) {
                throw new ColumnEncodeException(String.format("field reflection error: [FIELD: %s]", f.getName()), e);
            }
        }

        return result;
    }

    private int resetCount(Object obj, Field f, Column annotation) throws IllegalAccessException {
        return annotation.repeat() ? Integer.parseInt(String.valueOf(obj)) : 1;
    }
}
