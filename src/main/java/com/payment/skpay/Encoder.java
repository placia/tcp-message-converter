package com.payment.skpay;

public interface Encoder {
    int sizeOf(Object obj);
    void setLenValue(int lenValue, Object obj);
    String encode(Object obj);
}
